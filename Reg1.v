module Reg1(rst,clk,in,out,clear,ld);
    output reg out;
    input in,ld;
    input rst,clk,clear;
    
    always@(posedge clk,posedge rst) begin
        if(rst) out<=1'b0;
        else if(clear) out<= 1'b0;
        else if(ld) out <= in;
    end
endmodule
