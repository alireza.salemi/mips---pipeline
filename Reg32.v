module Reg32(rst,clk,in,out,clear,ld);
    output reg[31:0] out;
    input[31:0] in;
    input rst,clk,clear,ld;
    
    always@(posedge clk,posedge rst) begin
        if(rst) out<=32'b0;
        else if(clear) out<= 32'b0;
        else if(ld) out <= in; 
    end
endmodule
