module TB();
    reg clk = 1, rst = 1;
    DP dp(rst,clk);
    always #50 clk = ~ clk;
    initial begin
        #205 rst = 0;
    end
endmodule
