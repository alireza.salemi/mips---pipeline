module ALU(func,input1,input2,output1,zero);
    parameter SLT = 5 , ADD = 1 , SUB = 2, AND = 3 , OR = 4 , NOP = 0;
    input[2:0] func;
    input[31:0] input1,input2;
    output reg[31:0] output1;
    output zero;

    always@(func,input1,input2) begin
        case (func)
            SLT : output1 = input1 < input2 ? 32'b1 : 32'b0;
            ADD : output1 = input1 + input2;
            SUB : output1 = input1 - input2;
            AND : output1 = input1 & input2;
            OR : output1 = input1 | input2;
            NOP : output1 = output1;
            default: output1 = output1;
        endcase
    end
    assign zero = (output1==32'b0) ? 1'b1 : 1'b0;
endmodule