module MUX2_32(x0,x1,select,out);
    input[31:0] x0,x1;
    input select;
    output[31:0] out;
    assign out = select ? x1 : x0;
endmodule