module Reg3(rst,clk,in,out,clear,ld);
    output reg[2:0] out;
    input[2:0] in;
    input rst,clk,clear,ld;
    
    always@(posedge clk,posedge rst) begin
        if(rst) out<=3'b0;
        else if(clear) out<= 3'b0;
        else if(ld) out <= in; 
    end
endmodule
