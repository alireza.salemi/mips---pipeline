module DataMemory(addr,dataWrite,write,read,dataOut);
    input[31:0] addr;
    input[31:0] dataWrite;
    input write, read;
    output reg[31:0] dataOut;
    reg[31:0] mem[0:2047];

    initial begin
        $readmemb("data.txt",mem);
        dataOut = 32'b0;
    end
    always@(addr,write)begin
        if(write)begin
            mem[addr[10:0]] = dataWrite;
        end
        if(addr==32'bx)
            dataOut = 32'b0;
        else dataOut = mem[addr[10:0]];
    end
endmodule