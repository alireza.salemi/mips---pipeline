module InstructionMemory(addr,instruction);
    input[31:0] addr;
    output[31:0] instruction;
    reg[31:0] mem[0:2047];

    initial begin
        $readmemb("instructions.txt",mem);
    end
    assign instruction = mem[addr[10:0]];
endmodule