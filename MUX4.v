module MUX4_32(x0,x1,x2,x3,select,out);
    input[31:0] x0,x1,x2,x3;
    input[1:0] select;
    output[31:0] out;
    assign out = select==2'b00 ? x0 : select==2'b01 ? x1 : select == 2'b10 ? x2 : x3;
endmodule
