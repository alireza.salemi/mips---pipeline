module RegFile(clk,rst,addr1,addr2,addrWrite,write,dataWrite,output1,output2);
    wire[31:0] regs[0:31];
    reg actives[0:31];
    input clk,rst,write;
    input[4:0] addr1,addr2,addrWrite;
    input[31:0] dataWrite;
    output[31:0] output1,output2;
    assign output1 = regs[addr1];
    assign output2 = regs[addr2];    
    always@(addr1,addr2,addrWrite,write,dataWrite)begin : always1
        integer i;
        for(i=0;i<32;i=i+1)begin
            actives[i] = 1'b0;
        end
        if(write)begin
            if(addrWrite!=5'b0)begin
                actives[addrWrite] = 1'b1;
            end
        end
    end

    genvar j;
    generate for(j=0;j<32;j=j+1)begin : loop
        Reg32_regFile i(rst,clk,dataWrite,regs[j],actives[j]);
      end
    endgenerate

endmodule