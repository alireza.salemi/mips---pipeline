module DP(rst,clk);
    input rst,clk;

    //all wires 

    // PC 
    wire[31:0] ID_IF_PC_INPUT,ID_IF_PC_OUTPUT,ID_IF_PC_plus1;
    wire ID_IF_PC_clear,ID_IF_PC_load;
    wire[1:0] ID_IF_PC_MUX_select;
    
    // IF_ID
    wire[31:0] IF_ID_next_PC_wire,IF_ID_next_PC_INPUT_wire,IF_ID_instruction_INPUT_wire,IF_ID_instruction_wire;
    wire IF_ID_clear,IF_ID_next_PC_load,IF_ID_instruction_load,IF_ID_LD;
    
    //REG_FILE
    wire ID_EX_REGFILE_write;
    
    //ID_EX
    wire ID_EX_clear,ID_EX_brach_comparator_equal,ID_EX_DESTREG_WIRE;
    wire[4:0] ID_EX_RT_WIRE,ID_EX_RS_WIRE,ID_EX_RD_WIRE;
    wire[31:0] ID_EX_read_data1_OUTPUT,ID_EX_read_data2_OUTPUT,ID_EX_read_data1_INPUT,ID_EX_read_data2_INPUT,
        ID_EX_extended_addrress_wire,ID_EX_extended_addrress_input_wire,ID_EX_branch_prediction_wire,ID_EX_jump_Branch_wire,ID_EX_extended_wire;
    wire ID_EX_memread_temp_wire,ID_EX_memwrite_temp_wire,ID_EX_memToreg_temp_wire,ID_EX_regwrite_temp_wire;
    
    //controller that makes signals
    wire ID_EX_branch_signal,ID_EX_regWrite_signal,ID_EX_jump_signal,ID_EX_memWrite_signal,ID_EX_mem_to_reg_select_signal,ID_EX_memRead_signal,ID_EX_branch_not_equal_signal;
    wire[2:0] ID_EX_ALU_func_select_signal;
    
    //ALU
    wire EX_MEM_ALU_zero,EX_MEM_DEST_MUX_select,EX_MEM_clear,ID_EX_aluSrc_out;
    wire[4:0] EX_MEM_dest,EX_MEM_DEST_output;
    wire[2:0] EX_MEM_ALU_FUNC;
    wire[1:0] EX_MEM_ALU_SRC1_select,EX_MEM_ALU_SRC2_select;
    wire[31:0] EX_MEM_ALU_SRC1,EX_MEM_ALU_SRC2,EX_MEM_ALU_OUT,EX_MEM_DATA_WRITE_output,EX_MEM_ALU_temp;
    wire[31:0] EX_MEM_ALU_OUT_wire;
    wire EX_MEM_memToreg_temp_wire,EX_MEM_regwrite_temp_wire;
    
    //MEM access
    wire[31:0] MEM_WB_READ_DATA,MEM_WB_READ_DATA_reg_output,MEM_WB_ALU_result_reg_output;
    wire MEM_WB_Mem_write,MEM_WB_Mem_read,MEM_WB_clear;
    wire[4:0] MEM_WB_DEST_reg_output;
    
    //WB
    wire[31:0] WB_output;
    wire WB_select;
    

    //pc
    Reg32 PC(rst,clk,ID_IF_PC_INPUT,ID_IF_PC_OUTPUT,1'b0,ID_IF_PC_load);
    InstructionMemory instructionMem(ID_IF_PC_OUTPUT,IF_ID_instruction_INPUT_wire);
    Adder ID_IF_PC_adder(32'b1,ID_IF_PC_OUTPUT,ID_IF_PC_plus1);
    MUX4_32 ID_IF_PC_mux(ID_IF_PC_plus1,ID_EX_branch_prediction_wire,{IF_ID_next_PC_wire[31:26],IF_ID_instruction_wire[25:0]},ID_IF_PC_OUTPUT,ID_IF_PC_MUX_select,ID_IF_PC_INPUT);
    
    //IF_ID
    Reg32 IF_ID_next_PC(rst,clk,ID_IF_PC_plus1,IF_ID_next_PC_wire,IF_ID_clear,IF_ID_LD);
    Reg32 IF_ID_instruction(rst,clk,IF_ID_instruction_INPUT_wire,IF_ID_instruction_wire,IF_ID_clear,IF_ID_LD);
    
    //REG_FILE
    RegFile regFile(clk,rst,IF_ID_instruction_wire[25:21],IF_ID_instruction_wire[20:16],MEM_WB_DEST_reg_output,ID_EX_REGFILE_write,WB_output,ID_EX_read_data1_INPUT,ID_EX_read_data2_INPUT);

    //hazard detector unit

    hazardDetector hazard_unit(IF_ID_instruction_wire[25:21], IF_ID_instruction_wire[20:16], ID_EX_RT_WIRE, ID_EX_memread_temp_wire, ID_EX_jump_signal,ID_EX_branch_signal,ID_EX_branch_not_equal_signal,ID_EX_brach_comparator_equal, IF_ID_clear, ID_EX_clear, ID_IF_PC_load,IF_ID_LD,ID_IF_PC_MUX_select);
    
    //ID_EX
    Reg32 ID_EX_read_data1(rst,clk,ID_EX_read_data1_INPUT,ID_EX_read_data1_OUTPUT,ID_EX_clear,1'b1);
    Reg32 ID_EX_read_data2(rst,clk,ID_EX_read_data2_INPUT,ID_EX_read_data2_OUTPUT,ID_EX_clear,1'b1);
    Reg32 ID_EX_extend_imm(rst,clk,ID_EX_extended_addrress_input_wire,ID_EX_extended_wire,ID_EX_clear,1'b1);
    Reg1 ID_EX_aluSrc_reg(rst,clk,ID_EX_aluSrc,ID_EX_aluSrc_out,ID_EX_clear,1'b1);
    Comprator32 ID_EX_cmp(ID_EX_read_data1_INPUT,ID_EX_read_data2_INPUT,ID_EX_brach_comparator_equal);
    SignExtend8To16 ID_EX_extend(IF_ID_instruction_wire[15:0],ID_EX_extended_addrress_input_wire);
    Reg5 ID_EX_Rt(rst,clk,IF_ID_instruction_wire[20:16],ID_EX_RT_WIRE,ID_EX_clear,1'b1);
    Reg5 ID_EX_Rs(rst,clk,IF_ID_instruction_wire[25:21],ID_EX_RS_WIRE,ID_EX_clear,1'b1);
    Reg5 ID_EX_Rd(rst,clk,IF_ID_instruction_wire[15:11],ID_EX_RD_WIRE,ID_EX_clear,1'b1);
    Adder ID_EX_barnch_adder(IF_ID_next_PC_wire,ID_EX_extended_addrress_input_wire,ID_EX_branch_prediction_wire); // output should connect to another mux and then give it to jump_branch_eq in line 10 wich is commented
    // important !!! controller and regs for control signal leaved out. add it later
    
    Reg3 ID_EX_ALU_function_reg(rst,clk,ID_EX_ALU_func_select_signal,EX_MEM_ALU_FUNC,ID_EX_clear,1'b1);
    Reg1 ID_EX_MEMREAD_reg(rst,clk,ID_EX_memRead_signal,ID_EX_memread_temp_wire,ID_EX_clear,1'b1);
    Reg1 ID_EX_MEMWRITE_reg(rst,clk,ID_EX_memWrite_signal,ID_EX_memwrite_temp_wire,ID_EX_clear,1'b1);
    Reg1 ID_EX_MEM_to_reg_reg(rst,clk,ID_EX_mem_to_reg_select_signal,ID_EX_memToreg_temp_wire,ID_EX_clear,1'b1);
    Reg1 ID_EX_REGWRITE_reg(rst,clk,ID_EX_regWrite_signal,ID_EX_regwrite_temp_wire,ID_EX_clear,1'b1);
    Reg1 ID_EX_dest_reg(rst,clk,EX_MEM_DEST_MUX_select,ID_EX_DESTREG_WIRE,ID_EX_clear,1'b1);
    
    //controller that makes signals
    controller signal_controller(IF_ID_instruction_wire,ID_EX_aluSrc,ID_EX_branch_signal,ID_EX_regWrite_signal,EX_MEM_DEST_MUX_select,ID_EX_jump_signal, ID_EX_mem_to_reg_select_signal, ID_EX_memWrite_signal, ID_EX_memRead_signal,ID_EX_branch_not_equal_signal,ID_EX_ALU_func_select_signal);


    //ALU
    MUX3_32 EX_MEM_ALU_SRC1_mux(ID_EX_read_data1_OUTPUT,EX_MEM_ALU_OUT_wire,WB_output,EX_MEM_ALU_SRC1_select,EX_MEM_ALU_SRC1);
    MUX3_32 EX_MEM_ALU_SRC2_mux(ID_EX_read_data2_OUTPUT,EX_MEM_ALU_OUT_wire,WB_output,EX_MEM_ALU_SRC2_select,EX_MEM_ALU_temp);
    MUX2_32 EX_MEM_ALU_src(EX_MEM_ALU_temp,ID_EX_extended_wire,ID_EX_aluSrc_out,EX_MEM_ALU_SRC2);
    ALU EX_MEM_ALU(EX_MEM_ALU_FUNC,EX_MEM_ALU_SRC1,EX_MEM_ALU_SRC2,EX_MEM_ALU_OUT,EX_MEM_ALU_zero);
    MUX2_5 EX_MEM_DEST_MUX(ID_EX_RT_WIRE,ID_EX_RD_WIRE,ID_EX_DESTREG_WIRE,EX_MEM_dest);

    Reg32 EX_MEM_result_reg(rst,clk,EX_MEM_ALU_OUT,EX_MEM_ALU_OUT_wire,EX_MEM_clear,1'b1);
    Reg5 EX_MEM_Dest_reg(rst,clk,EX_MEM_dest,EX_MEM_DEST_output,EX_MEM_clear,1'b1);
    Reg32 EX_MEM_DATA_WRITE(rst,clk,EX_MEM_ALU_temp,EX_MEM_DATA_WRITE_output,EX_MEM_clear,1'b1);
    // important !!! data forwaard and regs for control signal leaved out. add it later

    Reg1 EX_MEM_MEMREAD_reg(rst,clk,ID_EX_memread_temp_wire,MEM_WB_Mem_read,MEM_WB_clear,1'b1);
    Reg1 EX_MEM_MEMWRITE_reg(rst,clk,ID_EX_memwrite_temp_wire,MEM_WB_Mem_write,MEM_WB_clear,1'b1);
    Reg1 EX_MEM_MEM_to_reg_reg(rst,clk,ID_EX_memToreg_temp_wire,EX_MEM_memToreg_temp_wire,MEM_WB_clear,1'b1);
    Reg1 EX_MEM_REGWRITE_reg(rst,clk,ID_EX_regwrite_temp_wire,EX_MEM_regwrite_temp_wire,MEM_WB_clear,1'b1);

    //forward unit
    ForwardUnit forwardUnit(ID_EX_RT_WIRE,ID_EX_RS_WIRE,EX_MEM_DEST_output,MEM_WB_DEST_reg_output,EX_MEM_regwrite_temp_wire,ID_EX_REGFILE_write,EX_MEM_ALU_SRC1_select,EX_MEM_ALU_SRC2_select);

    //mem access
    DataMemory dataMem(EX_MEM_ALU_OUT_wire,EX_MEM_DATA_WRITE_output,MEM_WB_Mem_write,MEM_WB_Mem_read,MEM_WB_READ_DATA);
    
    Reg32 MEM_WB_READ_DATA_reg(rst,clk,MEM_WB_READ_DATA,MEM_WB_READ_DATA_reg_output,MEM_WB_clear,1'b1);
    Reg32 MEM_WB_ALU_result_reg(rst,clk,EX_MEM_ALU_OUT_wire,MEM_WB_ALU_result_reg_output,MEM_WB_clear,1'b1);
    Reg5 MEM_WB_DEST_reg(rst,clk,EX_MEM_DEST_output,MEM_WB_DEST_reg_output,MEM_WB_clear,1'b1);

    //controll signals for wb

    Reg1 MEM_WB_MEM_to_reg_reg(rst,clk,EX_MEM_memToreg_temp_wire,WB_select,MEM_WB_clear,1'b1);
    Reg1 MEM_WB_REGWRITE_reg(rst,clk,EX_MEM_regwrite_temp_wire,ID_EX_REGFILE_write,MEM_WB_clear,1'b1);

    //WB
    MUX2_32 WB_MUX(MEM_WB_ALU_result_reg_output,MEM_WB_READ_DATA_reg_output,WB_select,WB_output);

endmodule