module Reg32_regFile(rst,clk,in,out,active);
    output[31:0] out;
    wire [31:0] dff1;
    input[31:0] in;
    input rst,clk,active;
    Reg32 register(rst,~clk,in,out,1'b0,active);
endmodule
