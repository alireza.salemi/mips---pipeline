module Comprator32(in1,in2,out);
    input[31:0] in1,in2;
    output out;
    assign out = (in1==in2);
endmodule