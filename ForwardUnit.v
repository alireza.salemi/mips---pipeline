module ForwardUnit(input[4:0] ID_EX_Rt,ID_EX_Rs,EX_MEM_R,MEM_WB_R,input EX_MEM_regWrite,MEM_WB_regWrite,output reg[1:0] src1,src2);
    always@(ID_EX_Rt,ID_EX_Rs,EX_MEM_R,MEM_WB_R)begin
       src1 = 2'b0;
       src2 = 2'b0;
       if((EX_MEM_regWrite == 1) && (EX_MEM_R == ID_EX_Rs) && (EX_MEM_R!=5'b0))begin
          src1 = 2'b01;
       end
       if((MEM_WB_regWrite == 1) && (MEM_WB_R == ID_EX_Rs) && (MEM_WB_R!=5'b0))begin
           src1 = 2'b10;
       end
       if((EX_MEM_regWrite == 1) && (EX_MEM_R == ID_EX_Rt) && (EX_MEM_R!=5'b0))begin
          src2 = 2'b01;
       end
       if((MEM_WB_regWrite == 1) && (MEM_WB_R == ID_EX_Rt) && (MEM_WB_R!=5'b0))begin
           src2 = 2'b10;
       end
    end
endmodule