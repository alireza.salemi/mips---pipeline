module MUX3_32(x0,x1,x2,select,out);
    input[31:0] x0,x1,x2;
    input[1:0] select;
    output[31:0] out;
    assign out = select==2'b00 ? x0 : select==2'b01 ? x1 : select == 2'b10 ? x2 : 32'bz;
endmodule