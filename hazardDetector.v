module hazardDetector(input [4:0] rs, rt, rd,input memRead, jump,beq,bne,equal, output clear_IF_ID, clear_ID_EX, PCLoad,IF_ID_LD, output [1:0] PCSrc);
    wire lwDependency;
    assign lwDependency = memRead & ((rs == rd) | (rt == rd));
    assign clear_IF_ID = ((beq & equal) | (bne & ~equal) | jump);
    assign IF_ID_LD = ~lwDependency;
    assign clear_ID_EX = lwDependency;
    assign PCLoad = 1'b1;
    assign PCSrc = (lwDependency) ? 2'b11 : (jump) ? 2'b10 : (beq & equal) | (bne & ~equal) ? 2'b01 : 2'b00;

endmodule