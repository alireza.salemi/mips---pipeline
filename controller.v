module controller(input[31:0] instruction, output reg ID_EX_aluSrc, branch, RegWrite, ID_EX_REGFILE_destination_MUX_select, jump, MemToReg, MemWrite, memRead, Bne, output reg[2:0]ALUOp);
    parameter LOAD = 35, STORE = 43, R_TYPE = 0, JUMP = 2, BEQ = 4, BNE = 5;
    parameter SLT = 42 , ADD = 32 , SUB = 34, AND = 36 , OR = 37 , NOP = 0;
    always@(instruction)begin
        {ID_EX_aluSrc, branch, RegWrite, jump, MemToReg, MemWrite, memRead,Bne,ID_EX_REGFILE_destination_MUX_select, ALUOp}=13'b0;
        case (instruction[31:26])
            LOAD: begin
            ID_EX_aluSrc = 1'b1;
                memRead = 1'b1;
                RegWrite = 1'b1;
                MemToReg = 1'b1;
                ALUOp = 3'b1;
                ID_EX_REGFILE_destination_MUX_select = 1'b0;
            end
            STORE:begin
            ID_EX_aluSrc = 1'b1;
                MemWrite = 1'b1;
                ALUOp = 3'b1;
            end
            R_TYPE:begin
            MemToReg = 1'b0;
                ID_EX_aluSrc = 1'b0;
                RegWrite = 1'b1;
                ID_EX_REGFILE_destination_MUX_select = 1'b1;
                case(instruction[5:0])
                    ADD : begin
                        ALUOp = 3'd1;
                    end
                    SUB : begin
                        ALUOp = 3'd2;
                    end
                    AND : begin
                        ALUOp = 3'd3;
                    end
                    OR : begin
                        ALUOp = 3'd4;
                    end
                    SLT : begin
                        ALUOp = 3'd5;
                    end
                    NOP : begin
                        {ID_EX_aluSrc, branch, RegWrite, jump, MemToReg, MemWrite, memRead,Bne,ID_EX_REGFILE_destination_MUX_select, ALUOp}=13'b0;
                    end
                    default : ALUOp = 3'd0;
                endcase
            end
            JUMP:begin
            ID_EX_aluSrc = 1'b0;
                jump = 1'b1;
            end
            BEQ:begin
                ID_EX_aluSrc = 1'b0;
                 branch = 1'b1;
            end
            BNE:begin
                ID_EX_aluSrc = 1'b0;
                Bne = 1'b1;
            end
            default: {ID_EX_aluSrc, branch, RegWrite, jump, MemToReg, MemWrite, memRead,Bne,ID_EX_REGFILE_destination_MUX_select, ALUOp}=13'b0;
        endcase
    end
endmodule

// R-Type: add, sub, and, or, slt
// Mem. Ref.: lw, sw
// Control Flow: j, beq, bne