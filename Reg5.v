module Reg5(rst,clk,in,out,clear,ld);
    output reg[4:0] out;
    input[4:0] in;
    input rst,clk,clear,ld;
    
    always@(posedge clk,posedge rst) begin
        if(rst) out<=5'b0;
        else if(clear) out<= 5'b0;
        else if(ld) out <= in; 
    end
endmodule
