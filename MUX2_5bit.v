module MUX2_5(x0,x1,select,out);
    input[4:0] x0,x1;
    input select;
    output[4:0] out;
    assign out = select ? x1 : x0;
endmodule